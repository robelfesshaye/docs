## mysql 

- check which mysql packages are installed
`rpm -qa | grep mysql`
`yum remove <package_from_above>`

- installation procedure: https://www.mysqltutorial.org/install-mysql-centos/

-   install phpmyadmin: `sudo yum install -y phpmyadmin`  
    source: https://www.youtube.com/watch?v=O9Q-zZ9h3EY 
