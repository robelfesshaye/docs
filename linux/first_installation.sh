$ subscription-manager register --username $username --password $password --auto-attach
$ yum update
## add min and maximize buttons to window  
$ yum -y install gnome-tweak-tool 
# make full screen    
$ yum -y install kernel-devel gcc make perl  
$ yum -y install elfutils-libelf-devel  
# install brew, awscli
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
$ brew install awscli
$ aws configure

#install rpm with dependencies  
$ yum localinstall x.rpm  

#git
$ sudo dnf install git-all
$ yum -y install docker 

## install monaco font
$ sudo mkdir -p /usr/share/fonts/truetype/ttf-monaco; cd /usr/share/fonts/truetype/ttf-monaco/  
$ sudo wget http://www.gringod.com/wp-upload/software/Fonts/Monaco_Linux.ttf 
$ sudo mkfontdir  
$ cd /usr/share/fonts/truetype/ 
$ fc-cache
$ sudo cp /usr/share/fonts/truetype/ttf-monaco/Monaco_Linux.ttf /usr/share/fonts/ 

## git branch on terminal -- add to ~/.bash_profile
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\u@\h \[\033[32m\]\w - \$(parse_git_branch)\[\033[00m\] $ "







