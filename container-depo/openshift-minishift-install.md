### openshift [ minishift ] installation steps  


1 - Enable the Red Hat Developer Tools software repository  
Run the following commands as the root user  
`$ su -`  
`# subscription-manager repos --enable rhel-7-server-devtools-rpms`  
`# subscription-manager repos --enable rhel-server-rhscl-7-rpms`  
  
Next, add the Red Hat Developer Tools key to your system. This allows yum to verify the integrity of the download packages:  

`# cd /etc/pki/rpm-gpg`  
`# wget -O RPM-GPG-KEY-redhat-devel https://www.redhat.com/security/data/a5787476.txt`  
`# rpm --import RPM-GPG-KEY-redhat-devel`  

2 - Install the CDK  
To install the CDK, run the following command as the root user:  

`# yum install cdk-minishift docker-machine-kvm`  
  
3 - Setup the CDK
`$ minishift setup-cdk` to setup the components needed to run CDK on your system. By default, minishift setup-cdk places CDK content in ~/.minishift. 
`$ minishift config set vm-driver virtualbox` [ make sure virutal-box is installed before running this command]    
  
4- Add your Red Hat Developer Program username to your environment  
`$ echo export MINISHIFT_USERNAME=$MINISHIFT_USERNAME >> ~/.bashrc`  
`$ source ~/.bashrc`  

5 - Start minishift  

`$ minishift start` this command starts a Red Hat Enterprise Linux VM with an OpenShift cluster running inside.  

After minishift start completes, make a note of:  
  
- The URL for the OpenShift cluster. Note: the IP address may change when you restart the CDK.  
- The developer username.  
- The admin username and default password.  

To check that everything is working and view the OpenShift console use the command:  
`$ minishift console`  
This launches the URL listed above (for example: https://192.168.42.60:8443) with your default browser. You will likely get a security warning from the browser about the certificate being from an unknown certificate authority. This is expected. You need to accept the certificate to proceed.  

To open the console in another browser, you can get the URL of the console with the following command:  

`$ minishift console --url`  

6 - Add the directory containing oc to your PATH  
After the minishift VM has been started, you need to add oc to your PATH. The oc command must match the version of the OpenShift cluster that is running inside of the Red Hat VM. The following command sets the correct version dynamically by running minishift oc-env and parsing the output.  

`$ eval $(minishift oc-env)` 

====  
Stopping minishift and the CDK life-cycle  
You can stop the minishift VM with the command:  
`$ minishift stop`  

You can restart it again with:  
`$ minishift start`  

If necessary, you can delete the VM to start over with a clean VM using:  
`$ minishift delete`  

** You won't need to run `minishift setup-cdk` again unless you delete the contents of ~/.minishift.  

- reference: https://developers.redhat.com/products/cdk/hello-world#fndtn-rhel  


## Errors when starting minishift
1 - ERROR = "Error during setting 'minishift' as active profile: The specified path to the kube config '/home/xyz/.minishift/machines/minishift_kubeconfig' does not exist"   
SOLUTION    
`minishift stop`  
`minishift delete --force --clear-cache`  
`mv ~/.kube ~/.kube.old`  
`minishift start --profile minishift`  
  
2 - ERROR = “VirtualBox is configured with multiple host-only adapters with the same IP”   
SOLUTION = `VBoxManage list -l hostonlyifs` to identify adapters, and then remove the one you don't need anymore `VBoxManage hostonlyif remove vboxnet1`
