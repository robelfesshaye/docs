- As root  
`CREATE USER 'robel'@'localhost' IDENTIFIED BY password;`  
`GRANT ALL PRIVILEGES ON * . * TO 'robel'@'localhost';`  
`FLUSH PRIVILEGES;`  

- As regular user  
`CREATE DATABASE habzil;`  

- Create tables `buy_get, sell_post, rent_get, rent_post`  
`CREATE TABLE buy_get ( id int AUTO_INCREMENT, Phone varchar(255), Address varchar(255), Description longtext, Bedrooms int, Country varchar(255), State varchar(255), PRIMARY KEY (id));`  

- Create tables `contact_messages`  
`CREATE TABLE contact_messages ( id int AUTO_INCREMENT, email varchar(255), description longtext, postdatetime datetime, PRIMARY KEY (id));`  




