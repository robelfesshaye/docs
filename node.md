## node issues

1 - `npm start` throws the following error `Internal watch failed: ENOSPC: System limit for number of file watchers reached, watch '/home/robel/habesha-zillow'`

solution: `echo fs.inotify.max_user_watches=582222 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`
source: https://stackoverflow.com/questions/34662574/node-js-getting-error-nodemon-internal-watch-failed-watch-enospc   

2 - connecting to db: `Client does not support authentication protocol requested by server; consider upgrading MySQL client',`
solution: https://stackoverflow.com/questions/50093144/mysql-8-0-client-does-not-support-authentication-protocol-requested-by-server

3- 